# README #

This repository contains C++ code for generating QED gauge field configurations on a discrete four-dimensional space-time lattice in either Feynman or Coulomb gauge. This code requires the [FFTW 3 library](http://www.fftw.org/) to be installed.

### Files ###

* main.cpp - Main function
* qedlib.cpp, qedlib.h - Classes and functions used in main.cpp
* params.h - Parameters
* stopwatch.hpp - Stopwatch class for timing code
* readgauge.cpp - Separate tool for reading a binary gauge configuration and printing in ASCII format

### Compiling and running ###

* Set parameters in params.h before compilation
* Compile and link main.cpp and qedlib.cpp, including headers qedlib.h, params.h and stopwatch.hpp
* Link math and FFTW3 libraries (-lm and -lfftw3 in g++)
* Required compiler flags: -std=c++11
* Recommended compiler flags: -O3
* To start random number generator from a saved state, include state file named RNGstart in output directory. Otherwise, generator will be seeded with the seed provided.

### Output (in specified output directory) ###

* File "RNGend" containing final state of the random number generator
* Text file RNGinfo.txt containing information about random number generator
* ASCII file containing Wilson loop values, with filename specified in params.h (only if flag WILSON in params.h is set to 1)
* ASCII file "plaquette.txt" containing plaquette values for final configuration (only if flag DO_PLAQUETTE in params.h is set to 1)
* Binary files containing field configurations, with filenames specified in params.h (only if flag SAVE_CONFIGS in params.h is set to 1)

### Binary file format ###

Header (512 bytes):  

* magic number (int) (to check endianness)
* Lattice size (int x4) (order: t,x,y,z)
* Gauge fixing (int) (1 = Coulomb gauge, 0 = Feynman gauge)
* Remainder of the header is currently unused (set to 0)

Data:  

* Vector potential values A[t, x, y, z, mu] (double) (fastest-moving index last) (mu = 0,1,2,3 = t,x,y,z)

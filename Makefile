SDIR=src
IDIR=src
ODIR=obj
BIN_DIR=bin

CC=g++
CFLAGS=-std=c++11 -O3 -I$(IDIR) -msse2 -mavx -Wall

LIBS=-lm -lfftw3

_DEPS = qedlib.h params.h stopwatch.hpp
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = main.o qedlib.o 
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: $(SDIR)/%.cpp $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BIN_DIR)/QED: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o $(BIN_DIR)/QED

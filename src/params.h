/*
params.h

Header file containing parameters for QED.cpp and main.cpp.

Author: James Harrison <jch1g10@soton.ac.uk>

Created: 16/11/2015
*/

#ifndef __PARAMS_GUARD__
#define __PARAMS_GUARD__

// Flags for options within code
#define COULOMB 0			// 1 for Coulomb gauge, 0 for Feynman gauge
#define TIMING 1 			// Time code?
#define WILSON 0 			// Compute Wilson loop expectation values?
#define SAVE_CONFIGS 1 		// Save configurations to file?
#define DO_PLAQUETTE 0 		// Compute plaquette?
#define ZERO_MODE_CHECK 0	// Check that zero modes were successfully removed?

// Lattice dimensions
// NOTE: Dimensions are ordered (t, x, y, z) throughout.
const int T = 64;								// Length of time dimension of lattice
const int L1 = 32;								// Length of x dimension of lattice
const int L2 = L1;								// Length of y dimension of lattice
const int L3 = L1;								// Length of z dimension of lattice

// Other parameters
const int NITER = 87;							// Number of configurations to generate
const char OUTPUTDIR[] = "./newoutput32x64_feynman";	// Directory for saving output files
const unsigned SEED = 252; 						// Seed for random number generator (only used when no RNGstart state file is present)

#if SAVE_CONFIGS
// Configuration output file parameters
const char CFILENAME[] = "U1config.bin";		// Filename for storing configurations (actual filenames will be CFILENAME.<trajectory number>)
const int TRAJSTART = 8690;						// Trajectory number of first configuration
const int TRAJSTEP = 40;						// Number of trajectories between each consecutive configuration
#endif // SAVE_CONFIGS

#if WILSON
// Wilson loop parameters
const int MAXWILSON = 10; 						// Maximum size of (square) Wilson loop to be computed
const char WFILENAME[] = "wilsonloops.dat";		// Filename for storing Wilson loop values
#endif // WILSON

#endif /* __PARAMS_GUARD__ */

/*
main.cpp

Generate QED field configurations on a four-dimensional discrete space-time
lattice.

Author: James Harrison <jch1g10@soton.ac.uk>

Created: 16/11/2015
Modified:
	25/11/2015 - Added computation of Wilson loops
	02/12/2015 - Added Coulomb-gauge option
	07/12/2015 - Added code for saving configurations to file
*/

#include <iostream>
#include <complex>
#include <fstream>
#include <cstdint>
#include <string>
#include <fftw3.h>
#include <string.h>
#include "qedlib.h"
#include "stopwatch.hpp"
#include "params.h"

const int HEADSIZE = 512;	// Size of configuration file header (in bytes)

using namespace std;


int main(){
	complex<double> *B;		// Array for storing configuration in momentum space
	double *B_real, *sigma;	// Arrays for storing real part of array B in position space, and standard deviations of B in momentum space

	// arrays for storing lattice momentum l_tilde_mu = 2*sin(l_mu/2)
	double l_tilde_0[T], l_tilde_1[L1], l_tilde_2[L2], l_tilde_3[L3];
	double * const l_tilde[4] = {l_tilde_0, l_tilde_1, l_tilde_2, l_tilde_3};
	// arrays for storing phase shifts exp(i*lbar_mu/2), for sawtooth momentum lbar_mu
	complex<double> phaseshift_0[T], phaseshift_1[L1], phaseshift_2[L2], phaseshift_3[L3];
	complex<double> * const phaseshift[4] = {phaseshift_0, phaseshift_1, phaseshift_2, phaseshift_3};

	Randgen generator; 				// Random number generator

	// Initialise random number generator
	generator.init();

#if SAVE_CONFIGS
	string cname;					// Filename for configuration output files
	ofstream cfile;					// File for storing configurations

	int trajnum = TRAJSTART;		// Trajectory number of configuration

	// Initialise header for configuration output files
	int *header = static_cast<int*>(malloc(HEADSIZE));
	int i_head = 0;	// Index of position within header

	int magic_number = 20103;
	header[i_head++] = magic_number;

	for(int mu = 0; mu < 4; mu++){
		header[i_head++] = DIMS[mu];
	}

	header[i_head++] = COULOMB;	// 1 if Coulomb gauge, 0 if Feynman gauge

	for(; i_head < HEADSIZE/sizeof(int); i_head++){
		header[i_head] = 0;
	}
#endif // SAVE_CONFIGS

#if WILSON
	ofstream wfile;					// File for storing Wilson loop expectation values
	string wfilename = OUTPUTDIR;	// Filename

	wfilename += '/';
	wfilename += WFILENAME;

	// Initialise file for storing Wilson loop values
	wfile.open(wfilename.c_str());
	for(int l_W = 1; l_W <= MAXWILSON; l_W++){
		wfile << l_W << '\t';
	}
	wfile << endl;
#endif // WILSON

#if DO_PLAQUETTE
	ofstream plaqfile;
	string plaqfilename = OUTPUTDIR;
	plaqfilename += "/plaquette.txt";

	plaqfile.open(plaqfilename.c_str());
	plaqfile << "Average plaquette\tSpace-like plaquette\tTime-like plaquette\tAverage link" << endl;
	plaqfile.close();
#endif // DO_PLAQUETTE

	// Allocate array for storing configuration
	B = static_cast<complex<double>*>(fftw_malloc(4*N * sizeof(complex<double>)));
	if(B == NULL){
		cerr << "Error: Failed to allocate memory for array 'B'." << endl;
		return EXIT_FAILURE;
	}

	// Allocate array for storing real part of array B in position space
	try{
		B_real = new double[4*N];
	}
	catch(bad_alloc&){
		cerr << "Error: Failed to allocate memory for array 'B_real'." << endl;
		return EXIT_FAILURE;
	}

	// Initialise arrays of lattice momentum 2*sin(l_mu/2)
	// and phase shifts exp(i*lbar_mu/2) for sawtooth momentum lbar_mu
	init_momentum(l_tilde, phaseshift);

	// Initialise array of standard deviations
	try{
		sigma = new double[N];
	}
	catch(bad_alloc&){
		cerr << "Error: Failed to allocate memory for array 'coeffs'." << endl;
		return EXIT_FAILURE;
	}

	init_sigma(sigma, l_tilde);

#if TIMING
	// Start timer
	Stopwatch timer;
	timer.start();
#endif // TIMING

	// Create FFTW plan
	fftw_plan plan = fftw_plan_many_dft(4 /* rank */, DIMS /* n */, 4 /* howmany */,
										reinterpret_cast<fftw_complex*>(B) /* in */, DIMS /* inembed */,
										4 /* istride */, 1 /* idist */,
										reinterpret_cast<fftw_complex*>(B) /* out */, DIMS /* onembed */,
										4 /* ostride */, 1 /* idist */,
										FFTW_BACKWARD /* sign */, FFTW_MEASURE /* flags */);

#if TIMING
	// Stop timer, print elapsed time, and restart
	timer.stop();
	cout << "FFTW plan time: " << timer.elapsed() << endl;
	timer.start();
#endif // TIMING


	for(int iter = 0; iter < NITER; iter++){
		cout << iter << endl;

		// Generate new momentum-space field configuration
#if COULOMB
		coulomb_config(B, sigma, l_tilde, phaseshift, &generator);
#else
		feynman_config(B, sigma, phaseshift, &generator);
#endif // COULOMB

		// Transform into position space
		fftw_execute(plan);

		// Normalise
		for(int i_B = 0; i_B < 4*N; i_B++){
			B[i_B] /= N;
		}

		// Note: Elements of B should now be real - ignore imaginary part in any further computation

		// Print all elements of B
		// int n_i[4];
		// int mu_i;
		// int N_i;
		// for(n_i[0] = 0; n_i[0] < DIMS[0]; n_i[0]++){
		// 	for(n_i[1] = 0; n_i[1] < DIMS[1]; n_i[1]++){
		// 		for(n_i[2] = 0; n_i[2] < DIMS[2]; n_i[2]++){
		// 			for(n_i[3] = 0; n_i[3] < DIMS[3]; n_i[3]++){
		// 				for(int mu = 0; mu < 4; mu++){
		// 					N_i = INDEX(n_i, mu);
		// 					if(mu==0){mu_i = 3;}
		// 					else{mu_i = mu-1;}
		// 					cout << n_i[1] << ' ' << n_i[2] << ' ' << n_i[3] << ' ' << n_i[0] << ' ' << mu_i << ' ' << B[N_i].real() << endl;
		// 				}
		// 			}
		// 		}
		// 	}
		// }

#if SAVE_CONFIGS
		// Get trajified output filename
		cname = OUTPUTDIR;
		cname += '/';
		cname += CFILENAME;
		cname += '.';
		cname += to_string(trajnum);

		// Open output file and write header to file
		cfile.open(cname.c_str(), ios::out | ios::binary);
		cfile.write((char*)header, HEADSIZE);

		// Write config data to file
		for(int i_B = 0; i_B < 4*N; i_B++){
			B_real[i_B] = B[i_B].real();
		}
		cfile.write((char*)B_real, 4*N*sizeof(double));

		cfile.close();
		trajnum += TRAJSTEP;
#endif // SAVE_CONFIGS

		/*
		// Output a few links, to check
		int mu;
		int n_index[4];
		cout << "(x,y,z,t) order" << endl;
		for(int i_mu = 0; i_mu < 4; i_mu++){
			if(i_mu == 3){mu = 0;}
			else{mu = i_mu + 1;}

			n_index[0] = n_index[1] = n_index[2] = n_index[3] = 0;
			cout << "coord = (" << n_index[1] << ',' << n_index[2] << ',' << n_index[3] << ',' << n_index[0] << ")," << i_mu << '\t' << B[INDEX(n_index, mu)].real() << endl;

			n_index[1] = 1;
			cout << "coord = (" << n_index[1] << ',' << n_index[2] << ',' << n_index[3] << ',' << n_index[0] << ")," << i_mu << '\t' << B[INDEX(n_index, mu)].real() << endl;
			n_index[1] = 0; n_index[2] = 1;
			cout << "coord = (" << n_index[1] << ',' << n_index[2] << ',' << n_index[3] << ',' << n_index[0] << ")," << i_mu << '\t' << B[INDEX(n_index, mu)].real() << endl;
			n_index[2] = 0; n_index[3] = 1;
			cout << "coord = (" << n_index[1] << ',' << n_index[2] << ',' << n_index[3] << ',' << n_index[0] << ")," << i_mu << '\t' << B[INDEX(n_index, mu)].real() << endl;
			n_index[3] = 0; n_index[0] = 1;
			cout << "coord = (" << n_index[1] << ',' << n_index[2] << ',' << n_index[3] << ',' << n_index[0] << ")," << i_mu << '\t' << B[INDEX(n_index, mu)].real() << endl;
			cout << endl;
		}
		*/

#if WILSON
		// Compute log of Wilson loops
		for(int l_W = 1; l_W <= MAXWILSON; l_W++){
			wfile << wilson_loop(B, l_W, l_W) << '\t';
		}
		wfile << endl;
#endif // WILSON

#if DO_PLAQUETTE
		// Compute plaquette, save in plaquette.txt
		plaquette(B);
#endif

#if ZERO_MODE_CHECK
		// Check that zero-mode removal was successful
		test_zero_mode(B);
#endif // ZERO_MODE_CHECK
	}

#if TIMING
	// Stop timer and print elapsed time
	timer.stop();
	cout << " Time for " << NITER << " iterations: " << timer.elapsed() << endl;
#endif // TIMING

	// Save final state of random number generator
	generator.save();

#if SAVE_CONFIGS
	// Close configuration file
	free(header);
#endif // SAVE_CONFIGS

#if WILSON
	// Close Wilson loop file
	wfile.close();
#endif // WILSON

	// Destroy FFTW plan
	fftw_destroy_plan(plan);

	// Free memory
	fftw_free(B);
	delete[] B_real;
	delete[] sigma;

	return 0;
}

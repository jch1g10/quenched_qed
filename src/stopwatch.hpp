/*
stopwatch.hpp

Defines a Stopwatch class, which can be used for timing code, using the
C++ <chrono> header.

Note: Function definitions are included in this header file.

Classes:
    Stopwatch - Class to be used as a stopwatch for timing code

Author: James Harrison <jch1g10@soton.ac.uk>

Created: 12/11/2015
*/

#ifndef __STOPWATCH_GUARD__
#define __STOPWATCH_GUARD__

#include <chrono>

using namespace std::chrono;

class Stopwatch{
  	/*
  	Class to be used as a stopwatch for timing code.
  	*/
private:
    time_point<high_resolution_clock> _begin, _end;					// Begin and end times of stopwatch
public:
    Stopwatch(void){start();}										// Construct and start the stopwatch
    void start(void){_begin = _end = high_resolution_clock::now();}	// Start the stopwatch
    void stop(void){_end = high_resolution_clock::now();}			// Stop the stopwatch
    double elapsed(void){											// Return elapsed time on stopwatch
	    duration<double> delta = duration_cast<duration<double>>(_end-_begin);
	    return delta.count();
    }
};

#endif /* __STOPWATCH_GUARD__ */

/*
qedlib.h

Header file for qedlib.cpp.

Author: James Harrison <jch1g10@soton.ac.uk>

Created: 16/11/2015
*/

#ifndef __QEDLIB_GUARD__
#define __QEDLIB_GUARD__

#define _USE_MATH_DEFINES

#include <iostream>
#include <random>
#include <cmath>
#include <complex>
#include <stdexcept>
#include <fstream>
#include <string>
#include "params.h"

// Set value of pi
#ifdef M_PI
	const double pi = M_PI;
#else
	const double pi = 3.14159265358979323846;
#endif

// Lattice dimensions
const int DIMS[4] = {T, L1, L2, L3};			// Dimensions of lattice
const int N = DIMS[0]*DIMS[1]*DIMS[2]*DIMS[3];	// Volume of lattice (total number of lattice points)

// Functions for finding array index from lattice indices
inline int INDEX(int n[4], int mu){
	return mu + 4*(n[3] + DIMS[3]*(n[2] + DIMS[2]*(n[1] + DIMS[1]*n[0])));
}
inline int INDEX(int n[4]){
	return n[3] + DIMS[3]*(n[2] + DIMS[2]*(n[1] + DIMS[1]*n[0]));
}

// Filenames for RNG state and info
const char RNGSTART[] = "RNGstart";
const char RNGEND[] = "RNGend";
const char RNGINFO[] = "RNGinfo.txt";


// Class prototypes
class Randgen{
	/*
	Wrapper class for random number generator, with public methods for
	generating double-precision floating-point random numbers with uniform or
	Gaussian distribution. Uniform random numbers are generated using the
	ranlux generator.
	*/
private:
	double _maxsize;				// Maximum value generated by base generator (for scaling purposes)
	std::ranlux48 _generator;		// Ranlux generator used for generating uniform random numbers
public:
	const std::string rngtype = "ranlux48";	// Type of RNG used
  	Randgen(void);					// Construct generator and set value of maxsize
	void gaussian(double*, int);	// Generate Gaussian random numbers
	void gaussian_complex(std::complex<double>*, int);	// Generate complex numbers with Gaussian random re and im parts
	void init(void);				// Load from saved state, if available; otherwise seed generator with SEED.
	double operator()(void); 		// Generate a single uniform random number in the interval [0, 1)
	void save(void);				// Save state of random number generator in file OUTPUTDIR/RNGEND
	void seed(unsigned);			// Seed ranlux generator
	void uniform(double*, int);		// Generate uniform random numbers in the interval [0, 1)
};


// Function prototypes
void coulomb_config(std::complex<double> *B, double *sigma, double * const l_tilde[4], std::complex<double> * const phaseshift[4], Randgen *gen);	// Generate a new random Coulomb-gauge field configuration
void feynman_config(std::complex<double> *B, double *sigma, std::complex<double> * const phaseshift[4], Randgen *gen);								// Generate a new random Feynman-gauge field configuration
void init_momentum(double * const l_tilde[4], std::complex<double> * const phaseshift[4]);	// Compute 2*sin(l_mu/2), and exp(i*lbar_mu/2) for sawtooth momentum lbar_mu
void init_sigma(double *sigma, double * const l_tilde[4]);									// Compute standard deviations of field configuration values
inline int modulo(int a, int b);															// Return a mod b, guaranteed to be >= 0
void plaquette(std::complex<double> *B);													// Compute the plaquette, save results in plaquette.txt
void test_zero_mode(std::complex<double> *B);												// Test whether zero-mode removal was successful
double wilson_loop(std::complex<double> *B, int L_r, int L_t);								// Compute the Wilson loop for one field configuration

#endif /* __QEDLIB_GUARD__ */

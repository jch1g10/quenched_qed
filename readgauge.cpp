#include <iostream>
#include <fstream>
#define HEADSIZE 512

using namespace std;

// Lattice dimensions
const int T = 64;
const int L1 = 32;
const int L2 = L1;
const int L3 = L1;

int main(){
	int mu_xyzt;
	double* B;

	char *header, *value;
	ifstream file("/Users/jamesharrison/Desktop/qed_stoch_test_feynman/u1configs_feynman/U1config.bin.5210", ios::in | ios::binary);

	if(file.is_open()){
		header = new char [HEADSIZE];
		value = new char [sizeof(double)];
		file.read(header, HEADSIZE);

		for(int t = 0; t < T; t++){
			for(int x = 0; x < L1; x++){
				for(int y = 0; y < L2; y++){
					for(int z = 0; z < L3; z++){
						for(int mu_txyz = 0; mu_txyz < 4; mu_txyz++){
							//N_i = INDEX(n_i, mu);
							if(mu_txyz==0){mu_xyzt = 3;}
							else{mu_xyzt = mu_txyz-1;}
							file.read(value, sizeof(double));
							B = (double*)value;
							cout << x << ' ' << y << ' ' << z << ' ' << t << ' ' << mu_xyzt << ' ' << *B << endl;
						}
					}
				}
			}
		}
		file.close();
		delete[] header;
		delete[] value;
	}
	else cout << "Unable to open file\n";
	return 0;
}